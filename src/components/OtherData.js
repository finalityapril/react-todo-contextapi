import React, { Component } from 'react'
import './styles/table.css'

class OtherData extends Component {

	constructor(props) {
		super(props)

		this.state={
			sampledata: []
		}
	}

	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/posts')
		.then((response) => response.json())
		.then((sampledata) => {
			this.setState({ sampledata: sampledata })
		})
	}

	render() {
		return (
			<div>
				<h3 style={{ 'textAlign':'center' }} > Sample Data </h3>
				<table>
					<thead>
						<tr>
							<th>ID</th>
							<th>Title</th>
							<th>Body</th>
						</tr>
					</thead>
					<tbody>
						{
							this.state.sampledata.map((data) => {
								return <tr key={ data.id }>
									<td>{ data.id }</td>
									<td>{ data.title }</td>
									<td>{ data.body }</td>
								</tr>
							})
						}
					</tbody>

				</table>

			</div>
		)
	}
}


export default OtherData