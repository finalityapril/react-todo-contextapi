import React, { Component, Fragment } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Header from './Header'
import OtherData from './OtherData'
import TodoForm from './TodoForm'
import TodoList from './TodoList'

import { TodoContext } from './TodoContext'

class App extends Component {
		
	constructor(props) {
    super(props)
		
		this.addTask = this.addTask.bind(this)  
		this.deleteTask = this.deleteTask.bind(this)
		this.clearAll = this.clearAll.bind(this)
		this.formSubmitHandler = this.formSubmitHandler.bind(this)
		this.taskChangeHandler = this.taskChangeHandler.bind(this)
		this.emailChangeHandler = this.emailChangeHandler.bind(this)
		this.dateChangeHandler = this.dateChangeHandler.bind(this)
	
		this.state = {
			list: [  {task: "Sample Task", email: "sample@email", date: "2019-09-14"} ],
			task: "",
			email: "",
			date: "",
			addTask: this.addTask,
			deleteTask: this.deleteTask,
			clearAll: this.clearAll,
			formSubmitHandler: this.formSubmitHandler,
			taskChangeHandler: this.taskChangeHandler,
			emailChangeHandler: this.emailChangeHandler,
			dateChangeHandler: this.dateChangeHandler,
		}
	}
	
	addTask() {
		this.setState(({ list, task, email, date }) => ({
			list: [ ...list,
				 {task: task, email:email, date:date }
			],
			task: "",
			email: "",
			date: "",
		}))
	}

	deleteTask (indexToDelete) {
		window.confirm('Are you sure you want to delete this item?') &&
		this.setState(({ list }) => ({
			list: list.filter((TodoForm, index) =>
				index!== indexToDelete)
		}))
	}

	clearAll(e) {
		if(this.state.list.length!==0) {
			window.confirm('Are you sure you want to clear everything?') &&
			this.setState({
				list: []
			})
		}
		else {
			alert('To-Do List is currently empty.')
		}
	}

	taskChangeHandler(e) {
		this.setState({
			task: e.target.value
		})
	}

	emailChangeHandler(e) {
		this.setState({
			email: e.target.value
		})
	}

	dateChangeHandler(e) {
		this.setState({
			date: e.target.value
		})
	}

	formSubmitHandler(e) {
		e.preventDefault()
		this.addTask()
		alert('Task details added.')
	}

	render() {

		return (
				<TodoContext.Provider value={this.state}>
					<Fragment>
						<BrowserRouter>
							<Header />
									<Switch>
										<Route exact path='/' component={ TodoForm }/>
										<Route exact path='/todo-list' component= { TodoList } />
										<Route exact path='/other-data' component={ OtherData }/>
									</Switch>
						</BrowserRouter>
					</Fragment>
				</TodoContext.Provider>
		)
	}
}

export default App