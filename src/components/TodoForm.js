import React, { Component } from 'react'
import {TodoContext} from './TodoContext'

class TodoForm extends Component {

	render() {

		return (
			<div style={ boxStyle }>
				<h2 style={ h2Style }>To-do Form</h2>
					<div style={{ 'position':'relative' }}>
					
						<form onSubmit={ this.context.formSubmitHandler }>
								<label style={{ 'color':'#fff', 'fontStyle':'italic' }}>Task</label>
								<input style={ inputStyle } type='text' 
									value = { this.context.task } onChange= { this.context.taskChangeHandler }
									placeholder='Enter task' minLength='4' required />

								<label style={{ 'color':'#fff', 'fontStyle':'italic' }}>Email</label>
								<input style={ inputStyle } type='email' 
									value = { this.context.email } onChange= { this.context.emailChangeHandler }
									placeholder='Enter email' minLength='5' required />

								<label style={{ 'color':'#fff', 'fontStyle':'italic' }}>Date</label>
								<input style={ inputStyle } type='date' 
									value = { this.context.date } onChange= { this.context.dateChangeHandler }
									placeholder='Enter email' min={Date.now()} required />

								<input style={ btnStyle } type='submit' />
						</form>

					</div>
			</div>
				
		)
	}

}

//Inline Style Variables
const boxStyle = {
	position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '25rem',
  padding: '2.5rem',
  boxSizing: 'borderBox',
  background: 'rgba(0, 0, 0, 0.6)',
  borderRadius: '0.625rem',
}

const h2Style = {
	margin: '0 0 1.875rem',
	padding: '0',
	color: '#fff',
	textAlign: 'center',
}

const inputStyle = {
	width: '100%',
  padding: '0.625rem 0',
  fontSize: '1rem',
  letterSpacing: '0.062rem',
  marginBottom: '1.875rem',
  border: 'none',
  borderBottom: '0.065rem solid #fff',
  outline: 'none',
  borderRadius: '0.625rem',
}

const btnStyle = {
	border: 'none',
  outline: 'none',
  color: '#fff',
  backgroundColor: '#1e3799',
  padding: '0.625rem 1.25rem',
  cursor: 'pointer',
  borderRadius: '0.312rem',
  fontSize: '1rem',
}

TodoForm.contextType = TodoContext
export default TodoForm