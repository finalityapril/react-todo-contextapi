import React, { Component } from 'react'
import { TodoContext } from './TodoContext'
import './styles/todolist.css'



class TodoList extends Component {


	render() {
		
		return (
			<div>

				<button className="clear-all" onClick={ this.context.clearAll.bind(this) }> Clear All </button>

				<div className="container">
				{
					this.context.list.map((item,key)=> {
						return(
							<div className="card-container" key={ key }>
								<div className="card-design">
									<p><i>Task:</i> { item.task }</p>
									<p><i>Email:</i> { item.email }</p>
									<p><i>Date:</i> { item.date }</p>
									<button className="delete-task" 
										onClick={ this.context.deleteTask.bind(this,key) }>
										Remove
									</button>
								</div>
							</div>
						)
					})
				} 
				</div>
			</div>
		)
	}
	
}

TodoList.contextType = TodoContext
export default TodoList