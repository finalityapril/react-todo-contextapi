import React from 'react'
import { Link } from 'react-router-dom'
import './styles/header.css'

const Header = (props) => {
	return (
		<div className="nav">
	  	<input type="checkbox" id="nav-check"/>
		  <div className="nav-header">
		    <div className="nav-title">
		      To-Do App
		    </div>
		  </div>
		  <div className="nav-btn">
		    <label htmlFor="nav-check">
		      <span></span>
		      <span></span>
		      <span></span>
		    </label>
		  </div>
		  
		  <div className="nav-links">
		    <Link to="/">To-Do Form</Link>
		    <Link to="todo-list">To-Do List</Link>
		    <Link to="other-data">Other Data</Link>
		  </div>
		</div>
	)
}

export default Header