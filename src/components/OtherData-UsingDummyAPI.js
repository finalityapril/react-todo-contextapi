import React, { Component } from 'react'
import './styles/table.css'

class OtherData extends Component {

	constructor(props) {
		super(props)

		this.state={
			employees: []
		}
	}

	componentDidMount() {
		fetch('http://dummy.restapiexample.com/api/v1/employees')
		.then((response) => response.json())
		.then((employees) => {
			this.setState({ employees: employees })
		})
	}

	render() {
		return (
			<div>
				<h3 style={{ 'textAlign':'center' }} >Employee List</h3>
				<table>
					<thead>
						<tr>
							<th>Employee ID</th>
							<th>Name</th>
							<th>Age</th>
							<th>Salary</th>
						</tr>
					</thead>
					<tbody>
						{
							this.state.employees.map((data) => {
								return <tr key={ data.id }>
									<td>{ data.id }</td>
									<td>{ data.employee_name }</td>
									<td>{ data.employee_age }</td>
									<td>{ data.employee_salary }</td>
								</tr>
							})
						}
					</tbody>

				</table>

			</div>
		)
	}
}


export default OtherData